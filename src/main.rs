#![feature(proc_macro_hygiene, decl_macro)]

use std::path::PathBuf;
use std::path::Path;
use rocket;
use rocket::response::NamedFile;
use rocket::routes;
use rocket::get;

#[get("/<path..>")]
fn serve(path:PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("site/").join(path)).ok()
}

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

fn main() {
    rocket::ignite().mount("/", routes![index,serve]).launch();
}
